from django import forms

class UserRegistration(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(max_length=64)

class CustomerRegistration(forms.Form):
    name = forms.CharField(max_length=64)
    address = forms.CharField(max_length=64)
    phoneNumber = forms.CharField(max_length=13)

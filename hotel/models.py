from django.db import models
from django.utils import timezone
from datetime import date
# Create your models here.

class State(models.Model):
    stateName = models.CharField(max_length=24, blank=False, null=False)
    def __str__(self):
        return self.stateName
    class Meta:
        ordering = ('stateName',)

class Hotel(models.Model):
    hotelName = models.CharField(max_length=24, blank=False, null=False)
    hotelAddress = models.CharField(max_length=255, blank=True, null=True)
    rank = models.IntegerField(blank=True, null=True)
    state = models.ManyToManyField(State)
    def __str__(self):
        return self.hotelName
    class Meta:
        ordering = ('rank',)

class PhoneNumber(models.Model):
    phoneNumber = models.CharField(max_length=13, blank=False, null=False)
    hotel = models.ManyToManyField(Hotel)
    def __str__(self):
        return self.phoneNumber

class Room(models.Model):
    hotel = models.ManyToManyField(Hotel)
    roomNumber = models.IntegerField(blank=False, null=False)
    maxOccupation = models.IntegerField(blank=False, null=False)
    bedQuantity = models.IntegerField(blank=False, null=False)
    pricePerDay = models.IntegerField(blank=False, null=False)
    def __str__(self):
        return self.roomNumber

class Service(models.Model):
    serviceName = models.CharField(max_length=64, blank=False, null=False)
    def __str__(self):
        return self.serviceName

class RoomService(models.Model):
    room = models.ManyToManyField(Room)
    service = models.ManyToManyField(Service)

class User(models.Model):
    email = models.EmailField()
    password = models.CharField(max_length=64, blank=False, null=False)
    role = models.CharField(max_length=64, blank=True, null=True)
    def __str__(self):
        return self.email

class Customer(models.Model):
    customerName = models.CharField(max_length=64, blank=False, null=False)
    customerPhoneNumber = models.CharField(max_length=13, blank=True, null=True)
    customerAddress = models.CharField(max_length=256, blank=True, null=True)
    def __str__(self):
        return self.customerName

class Reservation(models.Model):
    totalPrice = models.IntegerField(blank=False, null=False)
    startDate =  models.DateField(auto_now=False, auto_now_add=False)
    endDate =  models.DateField(auto_now=False, auto_now_add=False)
    customer = models.ManyToManyField(Customer)
    timestamp = models.DateField(auto_now=True)
    def __str__(self):
        return self.startDate

class RoomReservation(models.Model):
    occupation = models.IntegerField(blank=False, null=False)
    room = models.ManyToManyField(Room)
    reservation = models.ManyToManyField(Reservation)
    def __str__(self):
        return self.occupation

from django.contrib import admin

# Register your models here.
from .models import Hotel, State, User, PhoneNumber, Room, RoomService, Service, Customer, Reservation

class AdminHotel(admin.ModelAdmin):
    list_display = ["hotelName", "rank"]
    list_filter = ["rank"]
    list_editable = ["rank"]
    search_fields = ["hotelName"]
    class Meta:
        model = Hotel

class AdminState(admin.ModelAdmin):
    list_display = ["stateName"]
    list_filter = ["stateName"]
    search_fields = ["stateName"]
    class Meta:
        model = State

class AdminUser(admin.ModelAdmin):
    list_display = ["email"]
    search_fields = ["email"]
    class Meta:
        model = User


class AdminPhoneNumber(admin.ModelAdmin):
    list_display = ["phoneNumber"]
    class Meta:
        model = PhoneNumber

class AdminRoom(admin.ModelAdmin):
    list_display = ["roomNumber", "pricePerDay"]
    class Meta:
        model = Room

class AdminService(admin.ModelAdmin):
    list_display = ["serviceName"]
    class Meta:
        model = Service

class AdminRoomService(admin.ModelAdmin):
    class Meta:
        model = RoomService

class AdminCustomer(admin.ModelAdmin):
    list_display = ["customerName", "customerPhoneNumber"]
    class Meta:
        model = Customer

class AdminReservation(admin.ModelAdmin):
    list_display = ["startDate", "endDate", "totalPrice"]
    class Meta:
        model = Reservation

admin.site.register(Hotel, AdminHotel)
admin.site.register(State, AdminState)
admin.site.register(PhoneNumber, AdminPhoneNumber)
admin.site.register(User, AdminUser)
admin.site.register(Room, AdminRoom)
admin.site.register(Service, AdminService)
admin.site.register(RoomService, AdminRoomService)
admin.site.register(Customer, AdminCustomer)
admin.site.register(Reservation, AdminReservation)

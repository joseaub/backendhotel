from django.shortcuts import render
from .forms import UserRegistration
from .forms import CustomerRegistration
from .models import User
from .models import Customer
# Create your views here.
def index(request):
    return render(request, "index.html", {})


def userRegistration(request):
    form = UserRegistration(request.POST or None)
    if form.is_valid():
        user_data = form.cleaned_data
        email = user_data.get('email')
        password = user_data.get('password')
        store_user_data = User.objects.create(email=email, password=password)
    context= {
        "userform":form,
    }
    return render(request, "register.html", context)


def customerRegistration(request):
    form = CustomerRegistration(request.POST or None)
    if form.is_valid():
        customer_data = form.cleaned_data
        name = customer_data.get('name')
        address = customer_data.get('address')
        phone = customer_data.get('phone')
        store_user_data = Customer.objects.create(customerName=name, customerAddress=address, customerPhoneNumber=phone)
    context= {
        "customerform":form,
    }
    return render(request, "customerRegister.html", context)
